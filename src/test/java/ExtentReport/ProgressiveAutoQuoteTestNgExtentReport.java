package ExtentReport;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import Pages.ProgressiveAboutYourSelfClassObject;
import Pages.ProgressiveAboutYourVehicleClassObject;
import Pages.ProgressiveAutoQuoteClassObject;
import Pages.ProgressiveGetAQuoteClassObject;
import Pages.ProgressiveStartMyQuoteClassObject;

public class ProgressiveAutoQuoteTestNgExtentReport {

	// Global Variables
	ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	ExtentTest test;
	static WebDriver driver;

	@BeforeSuite

//	public void beforeSuit()
//
//	{
//
//		System.out.println("Before Suite method");
//	}
	public void setUpExtent() {
		// creating ExtentReporter object and creating new extentReport Html file
		// creating ExtentReports and attach the reporter(s)
		htmlReporter = new ExtentHtmlReporter("ProgressiveWebsite.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

	}

	@BeforeTest

	public void setUptest() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public static void navigateToProgressive() {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		driver = new ChromeDriver(options);
		driver.navigate().to("https://www.progressive.com/");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		ExtentTest BrowserOpened = extent.createTest("Invoke Browser",
				"This websites ensures that the browser is invoked");
		BrowserOpened.pass("Browser was invoked as Expected");

	}

	@Test(priority = 0) // Get a Auto Quote

	public static void ProgressiveAutoQuote() throws InterruptedException {
		// navigate to Progressive.com

		navigateToProgressive();

		ExtentTest test = extent.createTest("Progressive Homepage", "This Page ensures it is launched");
		test.log(Status.INFO, "Homepage Launched");

//		// creating a page object
//		ExtentTest test1 = extent.createTest("Progressive AutoQuote", "This page is launched");
//		test1.log(Status.INFO, "AutoQuote launched");

		// Creating WebElement Objects ProgressiveAutoQuote & Click AutoQuote

		ProgressiveAutoQuoteClassObject obj1 = new ProgressiveAutoQuoteClassObject(driver);
		Thread.sleep(1100);
		obj1.autoOnlyOption();
		test.pass("Click Auto Option");

	}

	@Test(priority = 1) // Input ZipCode and Get a Quote

	public static void ProgressiveGetAQuote() throws InterruptedException {

		ExtentTest test2 = extent.createTest("Progressive ZipCode Page",
				"This Page ensures that zipcode page is opened");
		test2.log(Status.INFO, "ZipCode Page");

		ProgressiveGetAQuoteClassObject obj2 = new ProgressiveGetAQuoteClassObject(driver);

		obj2.enterZipCode("30033");
		obj2.getAQuote();
		test2.pass("getAQuote");
	}

	@Test(priority = 2) // Input Personal Info

	public static void ProgressiveStartMyQuote() throws InterruptedException {
		
		ExtentTest test3 = extent.createTest("Progressive PersonalInfo","PersonalInfo page allows customers to fillout personal info");
		   test3.log(Status.INFO, "Personal Info Page Launched");

		ProgressiveStartMyQuoteClassObject obj3 = new ProgressiveStartMyQuoteClassObject(driver);

		obj3.validatingStartMyQuotePage();
		test3.pass("Validating StratMyQuote Page");
		obj3.enterFirstName("Aayan");
		test3.pass("Enter First Name");
		obj3.enterLastName("Shrestha");
		test3.pass("Enter Last Name");
		obj3.enterSuffix("Sr");
		test3.pass("Enter Suffix");
		obj3.enterDOB("07/12/1998");
		test3.pass("Enter DOB");
		obj3.enterStreetName("1000 Manin St");
		test3.pass("Enter Street Name");
		obj3.enterAptNumber("1002");
		test3.pass("Enter APT Number");
		obj3.cityBox("Decatur");
		test3.pass("Verify City ");
		obj3.zipCodeBox("30033");
		test3.pass("Verify Zip Code");
		obj3.poBoxChkBox();
		test3.pass("Check PO BOX");
		obj3.startMyQuote();
		test3.pass("startMyQuote");
		

	}

	@Test(priority = 3) // Input Vehicle Info

	public static void ArbinProgressiveAboutYourVehicle() throws InterruptedException

	{
		
		ExtentTest test4 = extent.createTest("Progressive Vehicle Page","This page will allows customers to add vehicle info");
		   test4.log(Status.INFO, "Progressive Vehicle Page Launched");
		
		
		ProgressiveAboutYourVehicleClassObject obj4 = new ProgressiveAboutYourVehicleClassObject(driver);
		Thread.sleep(5000);

		obj4.selectVehicleYear("2018");
		test4.pass("Vehicle Year Selected ");
		obj4.selectVehicleMake("Audi");
		test4.pass("Vehicle Make Selected ");
		obj4.selectVehicleModel("A7");
		test4.pass("Vehicle Model Selected ");
		obj4.primaryUse("Personal");
		test4.pass("Primary Use Enter ");
		obj4.vehicleRideSharingBox();
		test4.pass("Vehicle RideSharing Enter ");
		obj4.primaryZIPLocationBox("30033");
		test4.pass("Primary Zip Code Verify ");
		obj4.selectOwnOrLease("Own");
		test4.pass("Own/Lease Selected ");
		obj4.selectOwnershipDuration("1 month - 1 year");
		test4.pass("Ownership Duration Selected ");
		obj4.VehicleInfoDoneBtm();
		test4.pass("VehicleInfo DoneBtm ");
		obj4.continueToNextPage();        
		test4.pass("continueToNextPage");
		
		
	}

	@Test(priority = 4) // Input Residency and Driving History info
	public static void ArbinProgressiveAboutYourSelf() throws InterruptedException

	{
		ExtentTest test5 = extent.createTest("Progressive DrivingRecord Page","This page allows customers to add drivering info");
		   test5.log(Status.INFO, "Progressive Drivers Page Launched");
		
		
		ProgressiveAboutYourSelfClassObject obj5 = new ProgressiveAboutYourSelfClassObject(driver);

		obj5.selectMale();
		test5.pass("Gender Selected");
		obj5.maritalStatus("Single");
		test5.pass("MaritalStatus Selected");
		obj5.SocialSecurityNumber("566-15-5252");
		test5.pass("SocialSecurity Enetered");
		obj5.primaryResidence("Own home");
		test5.pass("primaryResidence Selected");
		obj5.MovedInLast2Months("No");
		test5.pass("MovedInfo Selected");
		obj5.USLicenseStatus("Valid");
		test5.pass("USLicenseStatus Entered");
		obj5.yearsLicensed("3 years or more");
		test5.pass("Number of License Years Entered");
		obj5.chooseClaims("");
		test5.pass("chooseClaims Selected");
		obj5.chooseTickets("");
		test5.pass("chooseTickets Selected");
		obj5.continueBtm();		
		test5.pass("continueBtm");
		
		

	}

	@AfterTest

	public static void terminateTest() throws InterruptedException {
		
		ExtentTest test6 = extent.createTest("Progressive Homepage","This page is closed");
        test6.log(Status.INFO, "Terminate the Browser");

		// Close the browser
		Thread.sleep(10000);
		driver.close();
		driver.quit();
       System.out.println(" Browser Closed");
	}

	@AfterMethod
	@AfterSuite
	public void tearDown() {
		extent.flush();
	}

	
	
	
}
