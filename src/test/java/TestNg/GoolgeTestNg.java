package TestNg;

import java.util.concurrent.TimeUnit; 

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GoolgeTestNg {

	static WebDriver driver = null;

	@BeforeTest

	public void setUptest() {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		driver = new ChromeDriver();

	}

	public static void navigateToGoogle() {

		driver.navigate().to("https://www.google.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test

	public static void performAction() {
		// navigate to Google.com
		 
		
		navigateToGoogle();

		// Creating Webelement Objects
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[contains(@name,'q')]"));
		WebElement searchBtn = driver.findElement(By.xpath("//input[contains(@name,'btnK')]"));

		// Utilizing the objects to perform Action

		txtBoxSearch.sendKeys("Wathat is TestNg");
		searchBtn.sendKeys(Keys.RETURN);

	}

	@AfterTest

	public static void terminateTest() throws InterruptedException {

		// Close the browser
		Thread.sleep(10000);
		driver.close();
		driver.quit();

	}

	
	 
}
