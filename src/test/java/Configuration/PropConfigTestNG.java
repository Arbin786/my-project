package Configuration;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PropConfigTestNG {
	
	
	static WebDriver driver = null;
	public static String browserName = null;
	

	@BeforeTest

	public void setUptest() {
		
		
		//Reads Configuration file and invokes driver based on the configuration

		//creating a obj of properties to get the properties currently configured		
		
		PropertiesFile.readProperties();	
			
			

			if(browserName.equalsIgnoreCase("chrome")) {

				System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

				driver = new ChromeDriver();

			} else if(browserName.equalsIgnoreCase("firefox")) {

				System.setProperty("webdriver.gecko.driver", ".\\libs\\geckodriver.exe");

				driver = new FirefoxDriver();

			}
			
			else if(browserName.equalsIgnoreCase("Internet Explorer")) 
			{

				System.setProperty("webdriver.ie.driver", ".\\libs\\IEDriverServer.exe");

				driver = new InternetExplorerDriver();
				
			}

		

	}

	public static void navigateToGoogle() {

		driver.navigate().to("https://www.google.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test

	public static void performAction() {
		// navigate to Google.com
		 
		
		navigateToGoogle();

		// Creating Webelement Objects
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[contains(@name,'q')]"));
		WebElement searchBtn = driver.findElement(By.xpath("//input[contains(@name,'btnK')]"));

		// Utilizing the objects to perform Action

		txtBoxSearch.sendKeys("Wathat is TestNg");
		searchBtn.sendKeys(Keys.RETURN);

	}

	@AfterTest

	public static void terminateTest() throws InterruptedException {

		// Close the browser
		Thread.sleep(10000);
		driver.close();
		driver.quit();

	}

	
	
	
	
	
	
	
	
	
	
	
	
	

}
