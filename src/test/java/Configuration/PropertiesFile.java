package Configuration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesFile {
	
	/*
	How to get data from properties file
	----------------------------------------------------------
	STEP 1: Create a object of class Properties class
	Properties prop = new Properties();

	STEP 2 : Create a object of class InputStream
	InputStream input = new FileInputStream("location of properties file");

	STEP 2 : Load Properties file
	prop.load(input);

	STEP 4 : Get values from Properties file
	prop.getProperty("browser");
	
	*/	
	//1. Create a object of class properties
	
	static Properties prop = new Properties();
	
	// Get the project path
	static String projectPath = System.getProperty("user.dir");
	
	
	public static void main(String[] args) {
		
		readProperties();
		
		writeInPropertiesFiles();
		
		readProperties();
		
	}
	
	
	
	
	public static void readProperties()
	
	{
		try {
			
			// 2. Create a object of class inputstream
			//InputStream input = new FileInputStream(projectPath + "/src/test/java/Configuration/Config.properties");
			InputStream input = new FileInputStream("C:\\Users\\ARBIN\\Desktop\\ProgressiveAutomationTest\\eclipse - Shortcut.lnk\\ProgressiveAutomation\\src\\test\\java\\Configuration\\Config.properties");
			
			//3.utilizing the properties obj so that we can load the found file
			
			prop.load(input);
			
			//4. get the values from the properties file
			String  browser = prop.getProperty("broswer");
			// Printing the check if correct value was received 
			System.out.println(browser + " was Invoked");
			
			// Set the browser for the particular 
			
			PropConfigTestNG.browserName = browser;
			System.out.println(PropConfigTestNG.browserName);
			
		}
			
			 catch (IOException e) {
				 
				 
				 System.out.println(e.getMessage());
					System.out.println(e.getCause());

					e.printStackTrace();
			 }
		
			
		}	
	
	/*
	 How to get data from properties file
----------------------------------------------------------
STEP 1: Create a object of class Properties class
Properties prop = new Properties();

STEP 2 : Create a object of class InputStream
InputStream input = new FileInputStream("location of properties file");

STEP 2 : Load Properties file
prop.load(input);

STEP 4 : Get values from Properties file
prop.getProperty("browser");

*/
	
	
	public static void writeInPropertiesFiles()
	
	{
		try {
			
		
		// set data to properties file
		// Create object of class output stream
		//create properties object
		OutputStream output = new FileOutputStream(projectPath + "/src/test/java/Configuration/Config.properties");
		// Setting the Values 
		
		
	   prop.setProperty("broswer", "IE");
		
		prop.setProperty("Result", "Pass");
		
		// Storing value in the properties file
		
		prop.store(output, "Browser Value Stored");
		
	}
	
	 catch (IOException e) {
		
		 
		 System.out.println(e.getMessage());
			System.out.println(e.getCause());

			e.printStackTrace();
		 
		 
		 
		 
		 
	 }
	 
		
		
	}
	
	
	
	
	
	
}		
			
			
			
			
	
		
		
		
		 
		
		
		
		
		
	
		
				
	


