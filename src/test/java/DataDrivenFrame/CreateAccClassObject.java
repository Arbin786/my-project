package DataDrivenFrame;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Excel.Utility.Xls_Reader;

public class CreateAccClassObject 
{

	WebDriver driver = null;

	WebElement element = null;

	By firstName = By.xpath("//input[@id='firstname']");

	By lastName = By.xpath("//input[@id='lastname']");

	By EmailAddress = By.xpath("//input[@id='email']");

	By password = By.xpath("//input[@id='PASSWORD']");

	// By createAccountBtm = By.xpath("//button[@type='submit']");

	public CreateAccClassObject(WebDriver driver) 
	{

		this.driver = driver;
	}

	public void sendData() throws InterruptedException 
	{
		// Get Data From Excel File

		Xls_Reader reader = new Xls_Reader(".\\Excel Files\\EbayCustomerRegisterInfo.xlsx");

		int rowCount = reader.getRowCount("Sheet1");

		// Parameterization :
		for (int rowNum = 2; rowNum <= rowCount; rowNum++) 
		{
			System.out.println("==============");
			String firstNameExcel = reader.getCellData("Sheet1", "FirstName", rowNum);

			System.out.println(firstName);

			String lastNameExcel = reader.getCellData("Sheet1", "LastName", rowNum);

			System.out.println(lastNameExcel);

			String EmailExcel = reader.getCellData("Sheet1", "Email", rowNum);

			System.out.println(EmailExcel);

			String passwordExcel = reader.getCellData("Sheet1", "Password", rowNum);

			System.out.println(passwordExcel);
			
			driver.findElement(firstName).clear();

			driver.findElement(firstName).sendKeys(firstNameExcel);

			TimeUnit.SECONDS.sleep(2);

			driver.findElement(lastName).clear();
			
			driver.findElement(lastName).sendKeys(lastNameExcel);

			TimeUnit.SECONDS.sleep(2);

			driver.findElement(EmailAddress).clear();
			
			driver.findElement(EmailAddress).sendKeys(EmailExcel);
			

			TimeUnit.SECONDS.sleep(2);

			driver.findElement(password).clear();
			driver.findElement(password).sendKeys(passwordExcel);

			TimeUnit.SECONDS.sleep(5);

		}

	}

}
