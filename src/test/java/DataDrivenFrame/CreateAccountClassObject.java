package DataDrivenFrame;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Excel.Utility.Xls_Reader;

public class CreateAccountClassObject {
	WebDriver driver = null;

	WebElement element = null;
	
	

	By firstName = By.xpath("//input[@id='firstname']");
	
	By lastName = By.xpath("//input[@id='lastname']");
	
	By EmailAddress = By.xpath("//input[@id='email']");
	
	By password = By.xpath("//input[@id='PASSWORD']");
	
	By createAccountBtm = By.xpath("//button[@type='submit']");

	public CreateAccountClassObject(WebDriver driver) {

		this.driver = driver;
}

	public void createAcccount() throws InterruptedException 
	{
		//Get Data From Excel File
		Xls_Reader reader = new Xls_Reader(".\\Excel Files\\EbayCustomerRegisterInfo.xlsx");
		
		
		String firstNameExcel = reader.getCellData("CreateAnAccount", "FirstName", 2);
		
		System.out.println(firstNameExcel);
		
        String lastNameExcel = reader.getCellData("CreateAnAccount", "LastName", 2);
		
		System.out.println(lastNameExcel);
		
		String EmailExcel = reader.getCellData("CreateAnAccount", "Email", 2);
		
		System.out.println(EmailExcel);
		
		
		String passwordExcel = reader.getCellData("CreateAnAccount", "Password", 2);
		
		System.out.println(passwordExcel);
		
		
		driver.findElement(firstName).sendKeys(firstNameExcel);
		
		driver.findElement(lastName).sendKeys(lastNameExcel);
		
		driver.findElement(EmailAddress).sendKeys(EmailExcel);
		
		driver.findElement(password).sendKeys(passwordExcel);
		
		TimeUnit.SECONDS.sleep(5);
		
		driver.findElement(createAccountBtm).sendKeys(Keys.ENTER);
		
		
		
		
		
		
	}

	
	
	
	
}	