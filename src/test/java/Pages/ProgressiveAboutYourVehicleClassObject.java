package Pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProgressiveAboutYourVehicleClassObject {
	WebDriver driver = null;

	WebElement element = null;

	

	
	//li[@class='ng-star-inserted selected']
	// li[@class='ng-star-inserted']//preceding::li[4]
	// li[@class='ng-star-inserted']//following::li[2]
	// (//li[contains(@class,'ng-star-inserted')])[4]
	// (//li[contains(@class,'ng-star-inserted')])[3]
	
	// *[@id=\"VehiclesNew_embedded_questions_list_Year\"]/ul/li[4]
	
	// By selectVehicleMake = By.cssSelector("#UnlistedVehicleFormModel_Make_Div");
	
	//ul[@tabindex='0']//li[4]
	
	//div[@class='ymm-label ng-star-inserted' and text()='Select vehicle year']
	//question[@class='ymm-dropdown year' and @property='Year']
	//ul[@class='ng-star-inserted']//li[@class='ng-star-inserted'][3]
	//*[@id=\"VehiclesNew_embedded_questions_list_Year\"]/ul/li[5]
	
	//By selectVehicleYear = By.xpath(" (//li[contains(@class,'ng-star-inserted')])[5]");
	
	By selectVehicleYear = By.xpath("/html[1]/body[1]/app-root[1]/block-ui[1]/block-ui[1]/form[1]/main[1]/vehicle-all[1]/vehicles-layout[1]/interview-layout-template[1]/div[1]/div[1]/div[1]/child-outlet-backend-scope[1]/sliding-child-router-outlet[1]/vehicle-details[1]/small-layout-child-template[1]/div[1]/div[1]/vehicle-shared-ymm-questions[1]/div[2]/div[1]/question[1]/label[1]/list-input[1]/ul[1]/li[5]");
	
	By selectVehicleMake = By.xpath("// li[contains(text(),'Audi')]");

	By selectVehicleModel = By.xpath("//li[contains(text(),'A7')]");


	By selectPrimaryUse = By.xpath("//select[@analytics-id='VehiclesNew_VehicleUse']");

	By selectvehicleRideSharingBox = By.xpath("//input[@type='checkbox']");

	By selectPrimaryZIPLocation = By.xpath("//input[@type='tel']");

	By selectOwnOrLease = By.xpath("//select[@analytics-id='VehiclesNew_OwnOrLease']");

	By selectOwnershipDuration = By.xpath("//select[@analytics-id='VehiclesNew_LengthOfOwnership']");

	By vehicleInfoDoneBtm = By.xpath("//loading-button[@data-automation-id='forwardNavigation']");

	By continueToNextPage = By.xpath("//button[text()='Continue']");

	public ProgressiveAboutYourVehicleClassObject(WebDriver driver) 
	{

		this.driver = driver;
	}

	

	public void selectVehicleYear(String vehYr) throws InterruptedException
	{
		
//		WebDriverWait wait = new WebDriverWait(driver, 10);    
//		wait.until(ExpectedConditions.elementToBeClickable(selectVehicleYear));
//		
		Thread.sleep(1200);
		driver.findElement(selectVehicleYear).click();
		
		       
		//driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
		//
		/*
		
		if(vehicleYr.contains("2019")) {
			driver.findElement(selectVehicleYear).click();
			
			System.out.println("Vehicle Year selected");
			Thread.sleep(1200);
		}
		else if(vehicleYr.contains("2010")) {
			System.out.println("Vehicle Year selected");
		}
			*/
		}

	public void selectVehicleMake(String vehicleMake) throws InterruptedException {

		List<WebElement> VehMk = driver.findElements(selectVehicleMake);
		for (WebElement veh : VehMk) {

			if (veh.getText().contains(vehicleMake)) {

				veh.click();
				System.out.println("Vehicle Make selected");
				Thread.sleep(1200);
			}
		}
	}

	public void selectVehicleModel(String vehiclemModel) throws InterruptedException {

		List<WebElement> VehModel = driver.findElements(selectVehicleModel);
		for (WebElement vehMd : VehModel) {

			if (vehMd.getText().contains(vehiclemModel)) {

				vehMd.click();
				System.out.println("Vehicle Model selected");
				Thread.sleep(1200);
			}
		}

	}

	public void primaryUse(String prmUse) throws InterruptedException {

		WebElement primUse = driver.findElement(selectPrimaryUse);

		if (primUse.getText().contains(prmUse)) {

			primUse.sendKeys(prmUse);
			Thread.sleep(1100);

		}

	}

	public void vehicleRideSharingBox() throws InterruptedException {

		WebElement RideSharingBox = driver.findElement(selectvehicleRideSharingBox);

		if (RideSharingBox.isSelected()) {
			System.out.println("VehicleRideSharing Checkbox is Toggled On");

		} else {
			System.out.println("VehicleRideSharing Checkbox is Toggled Off");
			RideSharingBox.click();
			Thread.sleep(1200);
		}

	}

	public void primaryZIPLocationBox(String primZipCode) throws InterruptedException {

		WebElement primaryZip = driver.findElement(selectPrimaryZIPLocation);
		if (primaryZip.getText().contains(primZipCode)) {

			System.out.println("PASS--Expected  ------Primary ZipCode is displayed as expected");
			Thread.sleep(1200);

		} else {
			System.out.println("Primary Zip Not Matching ---- Zip Failed");
			driver.findElement(selectPrimaryZIPLocation).clear();

			Thread.sleep(1200);

			driver.findElement(selectPrimaryZIPLocation).sendKeys(primZipCode);

			System.out.println(" Input Primary Zip Code Displayed-------- ");
			Thread.sleep(1200);
		}

	}

	public void selectOwnOrLease(String ownerShip) throws InterruptedException {

		WebElement ownLease = driver.findElement(selectOwnOrLease);

		if (ownLease.getText().contains(ownerShip)) {

			ownLease.sendKeys(ownerShip);
			System.out.println(" Own Or Lease Option Selected ------- ");
			Thread.sleep(1100);

		}

	}

	public void selectOwnershipDuration(String ownerShipDur) throws InterruptedException

	{
		WebElement ownerShipDuration = driver.findElement(selectOwnershipDuration);

		if (ownerShipDuration.getText().contains(ownerShipDur)) {

			ownerShipDuration.sendKeys(ownerShipDur);
			System.out.println(" OwnershipDuration Selected ------- ");
			Thread.sleep(1100);
			ScreenShotsClassObject.captureScreenShots(driver, "VehicleInfo Page");

		}
	}

	public void VehicleInfoDoneBtm() throws InterruptedException {
		
		driver.findElement(vehicleInfoDoneBtm).click();
		Thread.sleep(1100);
	}

	public void continueToNextPage() throws InterruptedException

	{
		
		driver.findElement(continueToNextPage).sendKeys(Keys.ENTER);
		Thread.sleep(1100);

	}

}
