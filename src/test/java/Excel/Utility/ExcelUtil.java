package Excel.Utility;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	
	public static  String path;
	
	public static void main(String[] args) {
		
		
	
		
		getRowCount();
		getCellDataString();
		getCellNumericValue();	
		

		
		
		
	}
	
	

	
	public static void getRowCount()
	{
		
	try 
	{
		// 1. create Excel file and add some data
		// 2. Create a reference for the Workbook(Excel File) a0nd provide the location of the workbook.
		
		//Create a workbook obj and refer to the excel file
		
		XSSFWorkbook wrkBk  = new XSSFWorkbook(".\\Excel Files\\AdminLogIn.xlsx");
		
		// Locate the Sheet in Excel file 
		XSSFSheet Sht = wrkBk.getSheet("Sheet1");
		// Call RowCount function and store in a int value
		
		int rowCount = Sht.getPhysicalNumberOfRows();
		
		
		//Print the rows in the sheet
		System.out.println("The Number of Rows : " +  rowCount);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
		catch(Exception e)
		
	{
		System.out.println(e.getMessage());	
		System.out.println(e.getCause());		
		e.printStackTrace();
			
	}
		
		
		
		
	}
	
	public static void getCellDataString()
	{
		try 
		{
			// 1. create Excel file and add some data
			// 2. Create a reference for the Workbook(Excel File) a0nd provide the location of the workbook.
			//Create a workbook obj and refer to the excel file
			
			XSSFWorkbook wrkBk  = new XSSFWorkbook(".\\Excel Files\\AdminLogIn.xlsx");
			
			// Locate the Sheet in Excel file 
			XSSFSheet Sht = wrkBk.getSheet("Sheet1");
			//Call function to get cell data of type String --> Specify the row and column
			//number based on
			// excel sheet
			
			String cellData = Sht.getRow(0).getCell(0).getStringCellValue();
			
		    
			
			// Print the cell data
			
			System.out.println("Cell Data Value is  :  "  +  cellData);
			
			
			
			
			
		}
		catch(Exception e)
		
		{
			System.out.println(e.getMessage());	
			System.out.println(e.getCause());		
			e.printStackTrace();
				
		}
		
		
		
	}
	
	public static void getCellNumericValue()
	
	{
		//Xls_Reader reader = new Xls_Reader(path);		
		
		try 
		{
			// 1. create Excel file and add some data
			// 2. Create a reference for the Workbook(Excel File) a0nd provide the location of the workbook.
			//Create a workbook obj and refer to the excel file
			
			XSSFWorkbook wrkBk  = new XSSFWorkbook(".\\Excel Files\\AdminLogIn.xlsx");
			
			// Locate the Sheet in Excel file 
			XSSFSheet Sht = wrkBk.getSheet("Sheet1");
			//Call function to get cell data of type String --> Specify the row and column
			//number based on
			// excel sheet
			
			double  cellDatas = Sht.getRow(1).getCell(1).getNumericCellValue();
			
			//reader.getCellData("Sheet1", "Password", 2);
			
			
			
			
			System.out.println("Cell Data Value is  :" +  cellDatas);
			
			
		}
		catch(Exception e)
		
		{
			System.out.println(e.getMessage());	
			System.out.println(e.getCause());		
			e.printStackTrace();
				
		}
		
		
		
		
		
		
		
	}
	
}
