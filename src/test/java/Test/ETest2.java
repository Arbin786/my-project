package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import DataDrivenFrame.CreateAccClassObject;
import DataDrivenFrame.CreateAccountClassObject;
import DataDrivenFrame.EbayHomePageClassObject;

public class ETest2 {

	

		private static WebDriver driver = null;

		public static void main(String[] args) throws InterruptedException {
			// step 1: Invoke Browser
			invokeBrowser();
			// Step 2 : Perform action on the web page
			ebayRegister();
			// step 3 : Close the Browser

			closeBrowser();

		}

		public static void invokeBrowser() {

			System.setProperty("webdriver.gecko.driver", ".\\libs\\geckodriver.exe");
	
			FirefoxOptions options = new FirefoxOptions();
			options.addArguments("incognito");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
			options.merge(capabilities);
			driver = new FirefoxDriver(options);
			driver.navigate().to("https://reg.ebay.com/reg/PartialReg?ru=https%3A%2F%2Fwww.ebay.com%2F/");
			System.out.println(driver.getTitle() + "------------------- Web Page Launched ");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();

		}

		public static void ebayRegister() throws InterruptedException {
			// Creating an object for EbayHomePageClassObject(HomePage) & Selecting Register Button

			CreateAccClassObject obj1 = new CreateAccClassObject(driver);
			
			obj1.sendData();
			
			
			

		}

		public static void closeBrowser() throws InterruptedException {
			// Close the browser
			Thread.sleep(2000);
			driver.close();
			driver.quit();

		}
	
}		
		
		
		
		
		
		
		
		
		
		
		



